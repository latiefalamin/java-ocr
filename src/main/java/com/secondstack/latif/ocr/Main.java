/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.latif.ocr;

import com.github.axet.lookup.Capture;
import com.github.axet.lookup.OCR;
import com.github.axet.lookup.OCRScale;
import java.io.File;

/**
 *
 * @author LATIF
 */
public class Main {

    public static void main(String[] args) {

        OCR l = new OCR(0.70f);

        // will go to com/github/axet/lookup/fonts folder and load all font
        // familys (here is only font_1 family in this library)
//        l.loadFontsDirectory(Main.class, new File("fonts"));
        l.loadFont(Main.class, new File("fonts//font2"));

        String str = "jjj";
        // recognize using all familys set
        str = l.recognize(Capture.load("kwh.png"));
        System.out.println(str);
        System.out.println("-----");
        str = l.recognize(Capture.load("test2.png"));
        System.out.println(str);

        // recognize using all familys set
//        str = l.recognize(Capture.load("D:\\Dropbox\\Icon\\coa reza\\coa si.png"));
//        System.out.println(str);
        System.out.println("------");

    }
}
